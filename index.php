<!doctype html>
<html lang="en" data-bs-theme="auto">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Crypto Hub Malta">
    <meta name="author" content="Crypto Hub Malta">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Crypto Hub Malta">
    <meta property="og:description" content="Crypto Hub Malta">
    <meta property="og:url" content="https://cryptohubmalta.org">
    <meta property="og:site_name" content="Crypto Hub Malta">
    <meta property="og:locale" content="en_US">

    <title>Crypto Hub Malta</title>

    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
    <title>Bootstrap Example</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script> -->

    <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@docsearch/css@3">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.css">

    <script src="assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/color-modes.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script>
      document.addEventListener("DOMContentLoaded", function(){
          var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
          var popoverList = popoverTriggerList.map(function(element){
              return new bootstrap.Popover(element);
          });
      });
    </script>



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .b-example-divider {
        width: 100%;
        height: 3rem;
        background-color: rgba(0, 0, 0, .1);
        border: solid rgba(0, 0, 0, .15);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
      }

      .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
      }

      .bi {
        vertical-align: -.125em;
        fill: currentColor;
      }

      .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
      }

      .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
      }

      .btn-bd-primary {
        --bd-violet-bg: #712cf9;
        --bd-violet-rgb: 112.520718, 44.062154, 249.437846;

        --bs-btn-font-weight: 600;
        --bs-btn-color: var(--bs-white);
        --bs-btn-bg: var(--bd-violet-bg);
        --bs-btn-border-color: var(--bd-violet-bg);
        --bs-btn-hover-color: var(--bs-white);
        --bs-btn-hover-bg: #6528e0;
        --bs-btn-hover-border-color: #6528e0;
        --bs-btn-focus-shadow-rgb: var(--bd-violet-rgb);
        --bs-btn-active-color: var(--bs-btn-hover-color);
        --bs-btn-active-bg: #5a23c8;
        --bs-btn-active-border-color: #5a23c8;
      }
      .bd-mode-toggle {
        z-index: 1500;
      }
    </style>
  </head>


  <body>
    <svg xmlns="http://www.w3.org/2000/svg" class="d-none">
      <symbol id="check2" viewBox="0 0 16 16">
        <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
      </symbol>
      <symbol id="circle-half" viewBox="0 0 16 16">
        <path d="M8 15A7 7 0 1 0 8 1v14zm0 1A8 8 0 1 1 8 0a8 8 0 0 1 0 16z"/>
      </symbol>
      <symbol id="moon-stars-fill" viewBox="0 0 16 16">
        <path d="M6 .278a.768.768 0 0 1 .08.858 7.208 7.208 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277.527 0 1.04-.055 1.533-.16a.787.787 0 0 1 .81.316.733.733 0 0 1-.031.893A8.349 8.349 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.752.752 0 0 1 6 .278z"/>
        <path d="M10.794 3.148a.217.217 0 0 1 .412 0l.387 1.162c.173.518.579.924 1.097 1.097l1.162.387a.217.217 0 0 1 0 .412l-1.162.387a1.734 1.734 0 0 0-1.097 1.097l-.387 1.162a.217.217 0 0 1-.412 0l-.387-1.162A1.734 1.734 0 0 0 9.31 6.593l-1.162-.387a.217.217 0 0 1 0-.412l1.162-.387a1.734 1.734 0 0 0 1.097-1.097l.387-1.162zM13.863.099a.145.145 0 0 1 .274 0l.258.774c.115.346.386.617.732.732l.774.258a.145.145 0 0 1 0 .274l-.774.258a1.156 1.156 0 0 0-.732.732l-.258.774a.145.145 0 0 1-.274 0l-.258-.774a1.156 1.156 0 0 0-.732-.732l-.774-.258a.145.145 0 0 1 0-.274l.774-.258c.346-.115.617-.386.732-.732L13.863.1z"/>
      </symbol>
      <symbol id="sun-fill" viewBox="0 0 16 16">
        <path d="M8 12a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z"/>
      </symbol>
    </svg>

    <div class="dropdown position-fixed bottom-0 end-0 mb-3 me-3 bd-mode-toggle">
      <button class="btn btn-bd-primary py-2 dropdown-toggle d-flex align-items-center"
              id="bd-theme"
              type="button"
              aria-expanded="false"
              data-bs-toggle="dropdown"
              aria-label="Toggle theme (auto)">
        <svg class="bi my-1 theme-icon-active" width="1em" height="1em"><use href="#circle-half"></use></svg>
        <span class="visually-hidden" id="bd-theme-text">Toggle theme</span>
      </button>
      <ul class="dropdown-menu dropdown-menu-end shadow" aria-labelledby="bd-theme-text">
        <li>
          <button type="button" class="dropdown-item d-flex align-items-center" data-bs-theme-value="light" aria-pressed="false">
            <svg class="bi me-2 opacity-50 theme-icon" width="1em" height="1em"><use href="#sun-fill"></use></svg>
            Light
            <svg class="bi ms-auto d-none" width="1em" height="1em"><use href="#check2"></use></svg>
          </button>
        </li>
        <li>
          <button type="button" class="dropdown-item d-flex align-items-center" data-bs-theme-value="dark" aria-pressed="false">
            <svg class="bi me-2 opacity-50 theme-icon" width="1em" height="1em"><use href="#moon-stars-fill"></use></svg>
            Dark
            <svg class="bi ms-auto d-none" width="1em" height="1em"><use href="#check2"></use></svg>
          </button>
        </li>
        <li>
          <button type="button" class="dropdown-item d-flex align-items-center active" data-bs-theme-value="auto" aria-pressed="true">
            <svg class="bi me-2 opacity-50 theme-icon" width="1em" height="1em"><use href="#circle-half"></use></svg>
            Auto
            <svg class="bi ms-auto d-none" width="1em" height="1em"><use href="#check2"></use></svg>
          </button>
        </li>
      </ul>
    </div>


    <header data-bs-theme="dark">
      <div class="collapse text-bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">

            <div class="col-sm-8 col-md-7 py-4">
              <h4>About Us</h4>
              <p class="text-body-secondary">
                We are <strong>Crypto Hub Malta</strong>, a free and warm blockchain-native community in Malta (Europe). Join us!
              </p>
            </div>

            <div class="col-sm-4 offset-md-1 py-4">
              <h4>Contact</h4>
              <div class="btn-group btn-group-sm">

                  <a class="btn btn-sm btn-outline-secondary" href="https://www.facebook.com/cryptohubmt/" target="_blank" class="text-white">Facebook</a>
                  <a class="btn btn-sm btn-outline-secondary" href="https://www.instagram.com/cryptohubmt/" target="_blank" class="text-white">Instagram</a>
                  <a class="btn btn-sm btn-outline-secondary" href="https://www.meetup.com/metaverse-nfts-defi/" target="_blank" class="text-white">Meetup</a>
                  <a class="btn btn-sm btn-outline-secondary" href="https://t.me/+OhPWSgdEcmc0YjVk"  target="_blank" class="text-white">Telegram</a>

              </div>
            </div>

          </div>
          <div class="row">

            <div class="col-sm-8 col-md-7 py-4">
              <h4>Resources</h4>
              <ul class="list-unstyled">
                <li>
                  <div class="btn-group btn-group-sm">
                    <a class="btn btn-sm btn-outline-secondary" href="http://101.cryptohubmalta.org" target="_blank" class="text-white">101 Forum</a>
                    <a class="btn btn-sm btn-outline-secondary" href="https://meet.jit.si" target="_blank" class="text-white">Jitsi videoconference</a>
                    <a class="btn btn-sm btn-outline-secondary" href="https://gitlab.com/crypto-hub-malta" target="_blank" class="text-white">Our repositories</a>
                  </div>
                </li>
              </ul>
            </div>

          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container">
          <a href="#" class="navbar-brand d-flex align-items-left fs-1"><strong>Crypto Ħub Malta</strong></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main>

      <section class="py-3 text-left container">
        <div class="row py-lg-1">
          <div class="col-lg-6 col-md-8 mx-auto">

            <!-- <img src="img/logo@2x_.png" alt="CHM Logo" class="img-fluid"> -->

            <p class="lead text-body-secondary">
              <ul class="list-unstyled">

                <li class="fs-5">&#8864; a vibrant and inclusive community</li>

                <li class="fs-5">&#8864; one public event every Wednesday</li>

                <!-- <li class="fs-5">&#8864; online study-groups every week</li> -->

              </ul>
          </div>
        </div>
      </section>

<?php
$filename = 'events/next/next.json';
$data = file_get_contents($filename);
$next_event = json_decode($data);
?>
      <div class="album py-5 bg-body-tertiary">
        <div class="container">
        <h4>Next event</h4>
          <!-- <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3"> -->
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-1 g-1">

            <div class="col">
              <div class="card shadow-sm">
                <!-- <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Event name" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em"><?= $next_event->event_name; ?></text></svg> -->
                <img src="<?= $next_event->event_teaser; ?>" alt="event teaser" class="img-fluid">
                <div class="card-body">
                  <h5 class="text-info"><i class="bi bi-calendar-check"></i> <?php $date=date_create($next_event->event_date); $new_date = date_format($date,"D F dS, Y"); echo $new_date; ?> at <?php echo $next_event->event_time; ?></h5>
                  <h5 class="card-text"><?= $next_event->event_name; ?></h5>
                  <p class="card-text"><span class="fst-italic">&laquo;<?= $next_event->event_description; ?>&raquo;</span></p>
                  <h6><a class="btn btn-sm btn-outline-secondary" href="<?= $next_event->event_location_url; ?>" target="_blank"><i class="bi bi-geo-alt">&nbsp;</i><?= $next_event->event_location_name ?></a></h6>
                  <?php
                  if (count($next_event->event_speeches) > 0) {
                    $text = "&Xi; Speech";
                    if (count($next_event->event_speeches) > 1) {
                      $text = "&Xi; Speeches";
                    }
                    echo '<br/><h4>' . $text . '</h4><br/>';
                    foreach($next_event->event_speeches as $key => $speech) {

                      if ($key > 0) {
                        echo '<br/>';
                      }
                      echo '<p class="card-text"><span class="fs-5">&rarr; </span><span class="fw-bold">' . $speech->speech_title . '</span>';

                      $speech_description='<div class="ms-4"><span class="fst-italic">&laquo;' . $speech->speech_description . '&raquo;</span>';
                      echo '<br/>'.$speech_description.'</div>';

                      // It's rare to have speech links before the event so I'm commenting this
                      // if (count($speech->speech_links) > 0) {
                      //   echo '<div class="d-flex justify-content-between align-items-center">
                      //           <div class="btn-group">';
                      //   foreach($speech->speech_links as $link) {
                      //     echo '<a class="btn btn-sm btn-outline-secondary" href="' . $link->url . '" target="_blank">' . $link->name . '</a>';
                      //   }
                      //   echo '</div></div>';
                      // }


                      if (count($speech->speech_speakers) > 1) {
                        $text = 'Speakers';
                      } else {
                        $text = 'Speaker';
                      }
                      echo '<div class="ms-4 mt-3 text-secondary"><h5>' . $text . '</h5></div>';

                      foreach ($speech->speech_speakers as $speaker) {

                        //bootstrap popover
                        $speaker_bubble = '<a tabindex="0" class="btn btn-sm btn-outline-secondary" role="button" data-bs-toggle="popover"
                          data-bs-trigger="focus" title="' . $speaker->speaker_name. '"
                          data-bs-content="' . $speaker->speaker_description . '"><i class="bi bi-person"></i>&nbsp;' . $speaker->speaker_name . '</a>';

                        $speaker_photo = '<img width="40" height="40" src="/assets/img/avatar.png"/>&nbsp;';
                        if ($speaker->speaker_photo_url != "") {
                          $speaker_photo = '<img width="40" height="40" src="'.$speaker->speaker_photo_url.'"/>&nbsp;';
                        }

                        $speaker_links = ' ';
                        if (count($speaker->speaker_links) > 0) {
                          foreach($speaker->speaker_links as $link) {
                            $slink = '<a class="btn btn-sm btn-outline-secondary" href="' . $link->url . '" target="_blank"><i class="bi bi-link"></i>&nbsp;' . $link->name . '</a> ';

                            if ($link->name == "telegram") {
                              $slink = '<a class="btn btn-sm btn-outline-secondary" href="' . $link->url . '" target="_blank"><i class="bi bi-telegram"></i>&nbsp;' . $link->name . '</a> ';
                            }

                            $speaker_links .= $slink;
                          }
                        }

                        echo '<div class="ms-4 mt-3 text-secondary"><span>' . $speaker_photo . $speaker_bubble . $speaker_links . '</span></div>';

                      }
                    }
                  }
                  ?>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

<?php
$filecount = count(glob('events/future/' . "*"));
if ($filecount > 0) {
  echo '
  <div class="album py-5 bg-body-primary">
  <div class="container">

    <h4>Future events</h4>
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-2">
    ';
}

$files = scandir('events/future/', SCANDIR_SORT_DESCENDING);

foreach($files as $filename) {

    if($filename == '.' || $filename == '..') continue;

    $data = file_get_contents('events/future/' . $filename);
    $event = json_decode($data);

    echo '
    <div class="col">
      <div class="card shadow-sm">
        <img src="' . $event->event_teaser .'" alt="event teaser" class="img-fluid">
        <div class="card-body">
          <h5 class="text-secondary"><i class="bi bi-calendar-check-fill"></i> ' . $event->event_date . '</h5>
          <p class="card-text text-secondary">' . $event->event_description . '</p>';

    if (count($event->event_speeches) > 0) {
      $text = "&Xi; Speech";
      if (count($event->event_speeches) > 1) {
        $text = "&Xi; Speeches";
      }
      echo '<h5>' . $text . '</h5>';
      foreach($event->event_speeches as $speech) {

        echo '<p class="card-text"><span class="fs-5">&rarr; </span><span class="fw-bold">' . $speech->speech_title . '</span> by ';

        foreach ($speech->speech_speakers as $speaker) {
          //bootstrap popover
          echo '<a tabindex="0" class="btn btn-sm btn-outline-secondary" role="button" data-bs-toggle="popover"
                data-bs-trigger="focus" title="' . $speaker->speaker_name. '"
                data-bs-content="' . $speaker->speaker_description . '">' . $speaker->speaker_name . '</a>';
        }
        echo '<br/><span class="fst-italic text-secondary">&laquo;' . $speech->speech_description . '&raquo;</span>';

        if (count($speech->speech_links) > 0) {
          echo '<div class="d-flex justify-content-between align-items-center">
                  <div class="btn-group">';
          foreach($speech->speech_links as $link) {
            echo '<a class="btn btn-sm btn-outline-secondary" href="' . $link->url . '" target="_blank">' . $link->name . '</a>';
          }
          echo '</div></div>';
        }
        echo '<br/></p>';
      }
    }

    echo '
      </div>
    </div>
  </div>';
  }

  if ($filecount > 0) {
    echo '
              </div>
            </div>
          </div>';
  }
?>


      <div class="album py-5 bg-body-primary">
        <div class="container">

          <h4>Past events</h4>
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-2">

<?php
$files = scandir('events/past/', SCANDIR_SORT_DESCENDING);
foreach($files as $filename) {

    if($filename == '.' || $filename == '..') continue;

    $data = file_get_contents('events/past/' . $filename);
    $event = json_decode($data);

    echo '
    <div class="col">
      <div class="card shadow-sm">
        <img src="' . $event->event_teaser .'" alt="event teaser" class="img-fluid">
        <div class="card-body">
          <h5 class="text-secondary"><i class="bi bi-calendar-check-fill"></i> ' . $event->event_date . '</h5>
          <p class="card-text text-secondary">' . $event->event_description . '</p>';

          if (count($event->event_speeches) > 0) {
            $text = "&Xi; Speech";
            if (count($event->event_speeches) > 1) {
              $text = "&Xi; Speeches";
            }
            echo '<h5>' . $text . '</h5>';
            foreach($event->event_speeches as $speech) {

              echo '<p class="card-text"><span class="fs-5">&rarr; </span><span class="fw-bold">' . $speech->speech_title . '</span> by ';

              foreach ($speech->speech_speakers as $speaker) {
                //bootstrap popover
                echo '<a tabindex="0" class="btn btn-sm btn-outline-secondary" role="button" data-bs-toggle="popover"
                      data-bs-trigger="focus" title="' . $speaker->speaker_name. '"
                      data-bs-content="' . $speaker->speaker_description . '">' . $speaker->speaker_name . '</a>';
              }
              echo '<br/><span class="fst-italic text-secondary">&laquo;' . $speech->speech_description . '&raquo;</span>';

              if (count($speech->speech_links) > 0) {
                echo '<div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">';
                foreach($speech->speech_links as $link) {
                  echo '<a class="btn btn-sm btn-outline-secondary" href="' . $link->url . '" target="_blank">' . $link->name . '</a>';
                }
                echo '</div></div>';
              }
              echo '<br/></p>';
            }
          }
    echo '

      </div>
    </div>
  </div>';
}
?>

          </div>
        </div>
      </div>

    </main>

    <footer class="text-body-secondary py-5">
      <div class="container">
        <p class="float-end mb-1">
          <a href="#">Back to top</a>
        </p>
        <!-- <p class="mb-1">Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
        <p class="mb-0">New to Bootstrap? <a href="/">Visit the homepage</a> or read our <a href="../getting-started/introduction/">getting started guide</a>.</p> -->
      </div>
    </footer>

  </body>
</html>
